const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const Dotenv = require("dotenv-webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
    mode: "development",
    entry: ["@babel/polyfill", "./src/index.js"],
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "bundle.js"
    },
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: "babel-loader",
                enforce: "pre"
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"]
            },
            {
                test: /\.(png|jpg|jpeg|gif)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: "[name].[ext]", // "[hash].[ext]" for hashed file names
                            outputPath: "img/"
                        }
                    }
                ]
            },
            {
                test: /\.(eot|otf|ttf)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            outputPath: "fonts/"
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./src/index.html",
            hash: true
        }),
        new Dotenv(),
        new MiniCssExtractPlugin({
            // Option similar to the same options in webpackOptions.output
            // both options are options
            filename: "[name].css"
        }),
        new CopyWebpackPlugin(
            [
                {
                    from:
                        ".well-known/acme-challenge/tgRyNw2QnS4fcYDe5gMqEi6uXvpprkxUibuEN6M0G1k/index.html",
                    to:
                        ".well-known/acme-challenge/tgRyNw2QnS4fcYDe5gMqEi6uXvpprkxUibuEN6M0G1k/"
                },
                {
                    from:
                        ".well-known/acme-challenge/q-3ZGItIFpruX6lNsejbKoauM9Wr8A4R2zOX6cBn5oI/index.html",
                    to:
                        ".well-known/acme-challenge/q-3ZGItIFpruX6lNsejbKoauM9Wr8A4R2zOX6cBn5oI/"
                }
            ],
            {
                ignore: [".DS_Store"]
            }
        )
    ]
};
