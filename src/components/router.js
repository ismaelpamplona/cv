// routing
import riot from "riot";
import route from "riot-route";

route.base("#/");
route.start(true);

route("/", function() {
    riot.mount("#content", "home");
});

route("/press", function() {
    riot.mount("#content", "press");
});
