import riot from "riot";
import "./navbar.scss";

import logoNav from "../../img/profile-isma.jpg";

riot.mixin("nav", {
    logoNav
});
