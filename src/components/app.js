// importing components / tags

import "./navbar/navbar";
import "./home/home";
import "./navfooter/navfooter";

// importing styling files
import "./app.scss";

// importing favicon
import favicon from "../img/favicon.png";
